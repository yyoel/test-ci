package mamikos.stepdef;

import cucumber.api.PendingException;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import mamikos.Profile;

import mamikos.WebUtility;
import static mamikos.WebUtility.*;

public class General {
    private Profile _profile = new Profile();

    @Given("^User access web mamikos$")
    public void user_access_web_mamikos() {
        getChromeInstance();
        redirectTo(_profile.getWebUrl());
    }

    @Then("^Close browser$")
    public void close_browser() throws Exception {
        closeBrowser();
    }
}
