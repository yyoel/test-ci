package mamikos.stepdef;

import static mamikos.WebUtility.*;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.junit.Assert;
import org.openqa.selenium.By;

public class Object {
    //dropdown
    @When("^User choose \"([^\"]*)\" on dropdown tipe kost$")
    public void user_choose_on_dropdown_tipe_kost(String tipeKost) {
        selectOption(By.id("kosGender"), tipeKost);
    }

    @When("^User choose \"([^\"]*)\" on dropdown jangka waktu$")
    public void user_choose_on_dropdown_jangka_waktu(String jangkaWaktu) {
        selectOption(By.id("kosRentType"), jangkaWaktu);
    }

    @Then("^System display \"([^\"]*)\" on dropdown urutkan berdasarkan$")
    public void system_display_on_dropdown_urutkan_berdasarkan(String urutkanBerdasarkan) {
        Assert.assertEquals(urutkanBerdasarkan, getValueDropdown(By.id("kosSorting")));
    }

    @When("^User choose \"([^\"]*)\" on dropdown urutkan berdasarkan$")
    public void user_choose_on_dropdown_urutkan_berdasarkan(String urutkanBerdasarkan) {
        selectOption(By.id("kosSorting"), urutkanBerdasarkan);
    }

    //edit text or text area

    @When("^User input \"([^\"]*)\" on field minimal price and \"([^\"]*)\" on field maximal price$")
    public void user_input_on_field_minimal_price_and_on_field_maximal_price(String minPrice, String maxPrice) {
        waitAndFill(By.id("filterPriceMin"), minPrice);
        waitAndFill(By.id("filterPriceMax"), maxPrice);
    }

    @When("^User click field search kost$")
    public void user_click_field_search_kost() {
        waitAndClick(By.id("mamiSearchField"));
    }

    //button

    @When("^User click button lokasi saya sekitar saya sekarang$")
    public void user_click_button_lokasi_saya_sekitar_saya_sekarang() {
        waitAndClick(By.xpath("//*[@id=\"app\"]/div/section[1]/div[3]/div/div[2]/div[1]/a"));
    }

    @When("^User click button saya mengerti on poppup bisa booking$")
    public void user_click_button_saya_mengerti_on_poppup_bisa_booking() {
        waitAndClick(By.id("popperConfirmation"));
    }

    @When("^User click button set$")
    public void user_click_button_set() {
        waitForAction(4000);
        waitAndClick(By.xpath("//*[@id=\"searchFilterPrice\"]/div[1]/form/div/button"));
    }
}
