package mamikos.stepdef;

import cucumber.api.java.en.Then;
import mamikos.function.Kost;

public class DisplayKost {

    private Kost _kost = new Kost();

    @Then("^System display kost with tipe kost \"([^\"]*)\" jangka waktu \"([^\"]*)\" price \"([^\"]*)\" to \"([^\"]*)\"$")
    public void system_display_kost_with_tipe_kost_jangka_waktu_price_to(String tipeKost, String jangkaWaktu, String minPrice, String maxPrice) {
        _kost.verifyKostList(tipeKost, jangkaWaktu, minPrice, maxPrice);
    }

    @Then("^System display kost with tipe kost \"([^\"]*)\" jangka waktu \"([^\"]*)\" price dari yang termurah$")
    public void system_display_kost_with_tipe_kost_jangka_waktu_price_dari_yang_termurah(String tipeKost, String jangkaWaktu) {
        _kost.verifyKostList(tipeKost, jangkaWaktu);
    }

    @Then("^System display kost with tipe kost \"([^\"]*)\" jangka waktu \"([^\"]*)\" urutan harga \"([^\"]*)\" price \"([^\"]*)\" to \"([^\"]*)\"$")
    public void system_display_kost_with_tipe_kost_jangka_waktu_urutan_harga_price_to(String tipeKost, String jangkaWaktu, String urutanHarga, String minPrice, String maxPrice) {
        _kost.verifyKostList(tipeKost, jangkaWaktu, urutanHarga, minPrice, maxPrice);
    }
}
