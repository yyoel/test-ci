package mamikos.function;

import static mamikos.WebUtility.*;

import org.junit.Assert;
import org.openqa.selenium.By;

public class Kost {
    public void verifyKostList(String tipeKost, String jangkaWaktu, String minPrice, String maxPrice) {
        waitForAction(5000);
        String attribute = "title";
        int foundKost  = 0;
        int countKost = Integer.parseInt(getValue(By.xpath("//*[@id=\"app\"]/div/div/div[3]/div[1]/div[1]/div[1]/h2/span")).replaceAll("[^\\d.]", ""));
        if(countKost > 20){
            countKost  = 20;
        }
        for (int i = 1; i <= countKost; i++) {
            Assert.assertEquals(getValue(By.xpath("//*[@id=\"app\"]/div/div/div[3]/div[1]/div[2]/div[2]/div[1]/div[" + i + "]/div/div[2]/div[1]/div[1]")), tipeKost);
            jangkaWaktu.toLowerCase().contains(getValue(By.xpath("//*[@id=\"app\"]/div/div/div[3]/div[1]/div[2]/div[2]/div[1]/div[" + i + "]/div/div[2]/div[2]/span/span")));
            int price = Integer.parseInt(getValueByAttribute(By.xpath("//*[@id=\"app\"]/div/div/div[3]/div[1]/div[2]/div[2]/div[1]/div[" + i + "]/div/div[2]/div[2]/span"), attribute).substring(3).replace(".", ""));
            if (price >= Integer.parseInt(minPrice) && price <= Integer.parseInt(maxPrice)) {
                foundKost++;
            }
        }
        Assert.assertEquals(countKost, foundKost);
    }

    public void verifyKostList(String tipeKost, String jangkaWaktu) {
        waitForAction(5000);
        String attribute = "title";
        int foundKost  = 0;
        actionBuilderToObject(By.xpath("//*[@id=\"app\"]/div/div/div[3]/div[1]/div[1]/div[1]/h2/span"));
        int countKost = Integer.parseInt(getValue(By.xpath("//*[@id=\"app\"]/div/div/div[3]/div[1]/div[1]/div[1]/h2/span")).replaceAll("[^\\d.]", ""));
        if(countKost > 20){
            countKost  = 20;
        }
        for (int i = 1; i < countKost; i++) {
            Assert.assertEquals(getValue(By.xpath("//*[@id=\"app\"]/div/div/div[3]/div[1]/div[2]/div[2]/div[1]/div[" + i + "]/div/div[2]/div[1]/div[1]")), tipeKost);
            jangkaWaktu.toLowerCase().contains(getValue(By.xpath("//*[@id=\"app\"]/div/div/div[3]/div[1]/div[2]/div[2]/div[1]/div[" + i + "]/div/div[2]/div[2]/span/span")));
            if(i>1){
                int price1 = Integer.parseInt(getValueByAttribute(By.xpath("//*[@id=\"app\"]/div/div/div[3]/div[1]/div[2]/div[2]/div[1]/div[" + (i-1) + "]/div/div[2]/div[2]/span"), attribute).substring(3).replace(".", ""));
                int price2 = Integer.parseInt(getValueByAttribute(By.xpath("//*[@id=\"app\"]/div/div/div[3]/div[1]/div[2]/div[2]/div[1]/div[" + (i) + "]/div/div[2]/div[2]/span"), attribute).substring(3).replace(".", ""));
                if (price1 <= price2) {
                    foundKost++;
                }
            }
        }
        Assert.assertEquals(countKost, (foundKost+2));
    }

    public void verifyKostList(String tipeKost, String jangkaWaktu,String  urutanHarga, String minPrice, String maxPrice) {
        waitForAction(5000);
        String attribute = "title";
        int countKost = Integer.parseInt(getValue(By.xpath("//*[@id=\"app\"]/div/div/div[3]/div[1]/div[1]/div[1]/h2/span")).replaceAll("[^\\d.]", ""));
        if(countKost != 0){
            int foundKost  = 1;
            if(countKost > 20){
                countKost  = 20;
            }
            for (int i = 1; i <= countKost; i++) {
                Assert.assertEquals(getValue(By.xpath("//*[@id=\"app\"]/div/div/div[3]/div[1]/div[2]/div[2]/div[1]/div[" + i + "]/div/div[2]/div[1]/div[1]")), tipeKost);
                jangkaWaktu.toLowerCase().contains(getValue(By.xpath("//*[@id=\"app\"]/div/div/div[3]/div[1]/div[2]/div[2]/div[1]/div[" + i + "]/div/div[2]/div[2]/span/span")));
                int price = Integer.parseInt(getValueByAttribute(By.xpath("//*[@id=\"app\"]/div/div/div[3]/div[1]/div[2]/div[2]/div[1]/div[" + i + "]/div/div[2]/div[2]/span"), attribute).substring(3).replace(".", ""));
                if (price >= Integer.parseInt(minPrice) && price <= Integer.parseInt(maxPrice)) {
                    if(i>1){
                        int price1 = Integer.parseInt(getValueByAttribute(By.xpath("//*[@id=\"app\"]/div/div/div[3]/div[1]/div[2]/div[2]/div[1]/div[" + (i-1) + "]/div/div[2]/div[2]/span"), attribute).substring(3).replace(".", ""));
                        int price2 = Integer.parseInt(getValueByAttribute(By.xpath("//*[@id=\"app\"]/div/div/div[3]/div[1]/div[2]/div[2]/div[1]/div[" + (i) + "]/div/div[2]/div[2]/span"), attribute).substring(3).replace(".", ""));
                        if (price1 >= price2) {
                            foundKost++;
                        }
                    }
                }
            }
            Assert.assertEquals(countKost, (foundKost));
        }
    }
}
