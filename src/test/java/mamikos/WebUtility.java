package mamikos;

import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.HashMap;
import java.util.Map;

import static org.openqa.selenium.support.ui.ExpectedConditions.visibilityOfElementLocated;

public class WebUtility<result> {
    private static ChromeDriver _driver;
    static {
        String os = System.getProperty("os.name").toLowerCase();
        //ChromeOptions options = new ChromeOptions();
        System.out.println(os);
        if(os.contains("mac")){
            System.setProperty("webdriver.chrome.driver", System.getProperty("user.dir") + "/bin/chromedriver_mac");
        }else if(os.contains("windows")){
            System.setProperty("webdriver.chrome.driver", System.getProperty("user.dir") + "/bin/chromedriver.exe");
        }else {
            //options.setBinary("/usr/bin/chrome");
            System.setProperty("webdriver.chrome.driver", System.getProperty("user.dir") + "/bin/chromedriver");
            //System.setProperty("webdriver.chrome.driver", System.getProperty("user.dir") + "/bin/chromedriver_lin");
        }
    }

    public static ChromeDriver getChromeInstance() {
        if (_driver == null) {
            Map<String, Object> prefs = new HashMap<String, Object>();
            prefs.put("profile.default_content_setting_values.notifications", 2);
            ChromeOptions options = new ChromeOptions();
            options.setExperimentalOption("prefs", prefs);
            options.setBinary("/usr/bin/google-chrome");
            options.setExperimentalOption("useAutomationExtension", false);
            //options.addArguments("--headless");
            //options.addArguments("--disable-gpu");
            options.addArguments("--start-fullscreen");
            _driver = new ChromeDriver(options);
        }
        return _driver;
    }

    public static String getCurrentURL() {
        String url = _driver.getCurrentUrl();
        return url;
    }

    public static void redirectTo(String url) {
        _driver.navigate().to(url);
    }

    public static void waitForElementVisible(By id) {
        WebDriverWait wait = new WebDriverWait(_driver, 20);
        wait.until(visibilityOfElementLocated(id));
    }

    public static void waitForAction(int millsec) {
        try {
            Thread.sleep(millsec);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public static void waitElementUsingTime(By id, int millSec) {
        WebDriverWait wait = new WebDriverWait(_driver, millSec);
        wait.until(visibilityOfElementLocated(id));
    }

    public static void waitAndClick(By id) {
        waitForElementVisible(id);
        waitForAction(500);
        _driver.findElement(id).click();
    }

    public static void scrollAndClick(By id) {
        ((JavascriptExecutor) _driver).executeScript("arguments[0].click();", _driver.findElement(id));
    }

    public static void scrollTo(By id) {
        WebElement element = _driver.findElement(id);
        ((JavascriptExecutor) _driver).executeScript("arguments[0].scrollIntoView(true);", element);
    }

    public static void waitAndFill(By id, String value) {
        waitForElementVisible(id);
        Actions actions = new Actions(_driver);
        actions.moveToElement(_driver.findElement(id));
        actions.click();
        actions.sendKeys(Keys.chord(Keys.CONTROL, "a"), value);
        actions.build().perform();
    }

    public static void selectOption(By id, String text) {
        Select option = new Select(_driver.findElement(id));
        waitForAction(300);
        option.selectByVisibleText(text);
    }

    public static String getValue(By id) {
        waitForElementVisible(id);
        scrollTo(id);
        waitForAction(300);
        String getValue = _driver.findElement(id).getText();
        return getValue;
    }

    public static String getValueByAttribute(By id, String attribute) {
        waitForElementVisible(id);
        waitForAction(300);
        String getValue = _driver.findElement(id).getAttribute(attribute);
        return getValue;
    }

    public static String getValueDropdown(By id) {
        waitForElementVisible(id);
        Select select = new Select(_driver.findElement(id));
        WebElement option = select.getFirstSelectedOption();
        String getValue = option.getText();
        return getValue;
    }

    public static void actionBuilderToObject(By id){
        Actions builder = new Actions(_driver);
        WebElement sideNav = _driver.findElement(id);
    }

    public static void closeBrowser() {
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        _driver.close();
    }
}
