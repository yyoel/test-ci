Feature: Search Kost
  Scenario: Search kost Khusus Putra, Harian, urutan Acak dan menggunakan Rentang Harga 50000 - 250000
    Given User access web mamikos
    When User click field search kost
    And User click button lokasi saya sekitar saya sekarang
    And User click button saya mengerti on poppup bisa booking
    And User choose "Khusus Putra" on dropdown tipe kost
    And User choose "Harian" on dropdown jangka waktu
    And User input "50000" on field minimal price and "250000" on field maximal price
    And User click button set
    Then System display "Acak" on dropdown urutkan berdasarkan
    And System display kost with tipe kost "Putra" jangka waktu "Harian" price "50000" to "250000"

  Scenario: Search kost Khusus Putra, Mingguan, urutkan dari Termurah
    When User choose "Khusus Putra" on dropdown tipe kost
    And User choose "Mingguan" on dropdown jangka waktu
    And User choose "Harga Termurah" on dropdown urutkan berdasarkan
    Then System display kost with tipe kost "Putra" jangka waktu "Mingguan" price dari yang termurah

  Scenario: Search kost Khusus Putri, Bulanan, urutkan dari Tertinggi
    When User choose "Khusus Putri" on dropdown tipe kost
    And User choose "Bulanan" on dropdown jangka waktu
    And User choose "Harga Termurah" on dropdown urutkan berdasarkan
    Then System display kost with tipe kost "Putri" jangka waktu "Mingguan" price dari yang termurah

  Scenario: Search kost Khusus Putri, Bulanan, urutkan dari Tertinggi
    When User choose "Campur" on dropdown tipe kost
    And User choose "Tahunan" on dropdown jangka waktu
    And User choose "Harga Tertinggi" on dropdown urutkan berdasarkan
    And User input "10000000" on field minimal price and "25000000" on field maximal price
    And User click button set
    Then System display kost with tipe kost "Campur" jangka waktu "Tahunan" urutan harga "Tertinggi" price "10000000" to "25000000"
    And Close browser
